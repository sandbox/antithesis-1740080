-- SUMMARY --

Very, very lightweight module that simply removes "Promoted to Front Page" and "Sticky at top of lists" options
from all forms.

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual
